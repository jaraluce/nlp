package me.relevante.nlp.core;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class IdfVector {

    private Map<String, Long> documentsCountByStem;
    private long totalDocumentsCount;

    public IdfVector() {
        this.documentsCountByStem = new HashMap<>();
        this.totalDocumentsCount = 0;
    }

    public double getIdfByStem(String stem) {
        Long totalDocumentsWithStem = documentsCountByStem.get(stem);
        if (totalDocumentsWithStem == null) {
            return 0;
        }
        return Math.log((double) totalDocumentsCount / (double) totalDocumentsWithStem);
    }

    public void addDocumentStems(List<String> stems) {
        for (String stem : stems) {
            Long documentCount = documentsCountByStem.get(stem);
            if (documentCount == null) {
                documentCount = 0L;
            }
            documentCount++;
            documentsCountByStem.put(stem, documentCount);
        }
        totalDocumentsCount++;
    }
}
