package me.relevante.nlp.core;

import org.apache.lucene.analysis.util.CharArraySet;
import org.apache.lucene.util.Version;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class EnglishStopWords {

	private static CharArraySet stopSet;
	private final static String STOP_WORDS_ENGLISH = "src/main/resources/stop_words_en.txt";

	public static CharArraySet getStopSet() {
		if (stopSet == null)
			stopSet = getStopWordsFromFile();
		return stopSet;
	}

	private static CharArraySet getStopWordsFromFile() {
		Path path = Paths.get(STOP_WORDS_ENGLISH);

	    try {
			
			List<String> stopSet = new ArrayList<>();
			Files.lines(path).forEach(s -> stopSet.add(s));
			
			CharArraySet aux = new CharArraySet(Version.LUCENE_4_9,stopSet ,false);
			return aux;
				
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;

	}

}
