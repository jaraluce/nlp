package me.relevante.nlp.core;

public interface NlpEntity {
    String getNlpAnalyzableContent();
}
