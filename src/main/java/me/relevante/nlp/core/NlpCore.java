package me.relevante.nlp.core;

import com.cybozu.labs.langdetect.Detector;
import com.cybozu.labs.langdetect.DetectorFactory;
import com.cybozu.labs.langdetect.LangDetectException;
import org.apache.commons.lang3.StringUtils;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.core.LowerCaseFilter;
import org.apache.lucene.analysis.core.StopFilter;
import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.analysis.en.PorterStemFilter;
import org.apache.lucene.analysis.es.SpanishAnalyzer;
import org.apache.lucene.analysis.miscellaneous.ASCIIFoldingFilter;
import org.apache.lucene.analysis.shingle.ShingleFilter;
import org.apache.lucene.analysis.standard.ClassicFilter;
import org.apache.lucene.analysis.standard.ClassicTokenizer;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.util.CharArraySet;
import org.apache.lucene.util.Version;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.StringReader;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Component
public class NlpCore {

    private static final Logger logger = LoggerFactory.getLogger(NlpCore.class);
    private static final String LANGUAGE_PROFILES = "src/main/resources/profiles";
    private static final String DEFAULT_LANGUAGE = "en";

    private Map<String, CharArraySet> defaultStopSets;
    private Map<String, CharArraySet> customStopSets;
    private Detector detector;


    public NlpCore() throws Exception {
        try {
            DetectorFactory.loadProfile(LANGUAGE_PROFILES);
            this.detector = DetectorFactory.create();
            this.defaultStopSets = new HashMap<>();
            this.customStopSets = new HashMap<>();
            defaultStopSets.put("en", EnglishAnalyzer.getDefaultStopSet());
            customStopSets.put("en", EnglishStopWords.getStopSet());
            defaultStopSets.put("es", SpanishAnalyzer.getDefaultStopSet());
            customStopSets.put("es", SpanishStopWords.getStopSet());
        } catch (LangDetectException e) {
            logger.error("Error", e);
        }
    }

    public LinkedHashMap<String, Keyword> extractKeywords(String text) {
        return extractKeywords(text, (IdfVector) null);
    }

    public LinkedHashMap<String, Keyword> extractKeywords(String text,
                                                          IdfVector idfVector) {
        String detectedLanguage = detectLanguage(text);
        LinkedHashMap<String, Keyword> keywords = extractKeywords(text, idfVector, detectedLanguage);
        return keywords;
    }

    public LinkedHashMap<String, Keyword> extractKeywords(String text,
                                                          String language) {
        return extractKeywords(text, null, language);
    }

    public LinkedHashMap<String, Keyword> extractKeywords(String text,
                                                          IdfVector idfVector,
                                                          String language) {
        String cleanText = cleanInput(text);
        List<String> terms = extractTerms(cleanText, language);
        LinkedHashMap<String, Keyword> keywords = convertTermsIntoKeywords(terms);
        for (String stem : keywords.keySet()) {
            Keyword keyword = keywords.get(stem);
            double tf = keyword.getTerms().size();
            double idf = 1.0;
            if (idfVector != null) {
                idf = idfVector.getIdfByStem(keyword.getStem());
            }
            keyword.setTfIdf(tf, idf);
        }

        return keywords;
    }

    public List<String> extractStemsFromKeywords(List<Keyword> keywords) {
        List<String> stems = new ArrayList<>(keywords.size());
        keywords.forEach(keyword -> stems.add(keyword.getStem()));
        return stems;
    }

    public List<String> extractFirstTermsFromKeywords(List<Keyword> keywords) {
        List<String> terms = new ArrayList<>(keywords.size());
        keywords.forEach(keyword -> terms.add(keyword.getTerms().iterator().next()));
        return terms;
    }

    public List<String> extractMostPopularTerms(String text,
                                                int maxTerms) {
        Map<String, Keyword> keywordsByStem = extractKeywords(text);
        List<Keyword> keywords = new ArrayList<>(keywordsByStem.values());
        keywords.sort((o1, o2) -> Double.valueOf(o2.getTf()).compareTo(Double.valueOf(o1.getTf())));

        Set<String> relatedTerms = new LinkedHashSet<>();
        Set<String> forbiddenTerms = new HashSet<>();
        int i = 0;
        for (Keyword keyword : keywords) {
            String term = keyword.getTerms().get(0);
            if (term.contains("+")) {
                String[] singleTerms = term.split("\\+");
                for (String singleTerm : singleTerms) {
                    relatedTerms.remove(singleTerm);
                    forbiddenTerms.add(singleTerm);
                }
            }
            if (forbiddenTerms.contains(term))
                continue;
            relatedTerms.add(term);
            forbiddenTerms.add(term);
            i++;
            if (i == maxTerms)
                break;
        }
        return new ArrayList<>(relatedTerms);
    }

    public String detectLanguage(String text) {
        detector.append(StringUtils.defaultString(text));
        String language = null;
        try {
            language = detector.detect();
        } catch (LangDetectException e) {
            logger.error("Language Detection Error", e);
        }
        if (!defaultStopSets.containsKey(language)) {
            language = null;
        }
        return language;
    }

    public String cleanInput(String input) {

        String cleanInput = Normalizer.normalize(input, Normalizer.Form.NFD);
        cleanInput = cleanInput.replaceAll("\\p{M}", "")
                // replace any punctuation char but apostrophes, dashes, and commas by a space
                .replaceAll("[^\\w^\\d^-^\\'^,-]", " ")
                        // replace most common english contractions
                .replaceAll("(?:'(?:[tdsm]|[vr]e|ll))+\\b", "");
        // remove short words and characters
        cleanInput = cleanInput.replaceAll("\\b\\D\\b", " ");
        return cleanInput;
    }

    public List<String> extractTerms(String input,
                                     String language) {

        // tokenize input
        TokenStream tokenStream = new StandardTokenizer(Version.LUCENE_4_9, new StringReader(input));
        // to lowercase
        tokenStream = new LowerCaseFilter(Version.LUCENE_4_9, tokenStream);

        // remove dots from acronyms (and "'s" but already done manually above)
        tokenStream = new ClassicFilter(tokenStream);
        // convert any char to ASCII
        tokenStream = new ASCIIFoldingFilter(tokenStream);

        Set<String> languages = new HashSet<>();
        if (language == null) {
            languages.addAll(defaultStopSets.keySet());
        } else {
            languages.add(language);
        }
        for (String lang : languages) {
            // language default stop words
            tokenStream = new StopFilter(Version.LUCENE_4_9, tokenStream, defaultStopSets.get(lang));
            // language customized stop words
            tokenStream = new StopFilter(Version.LUCENE_4_9, tokenStream, customStopSets.get(lang));
        }
        // customized stop words
        tokenStream = new StopFilter(Version.LUCENE_4_9, tokenStream, CustomStopWords.getStopSet());

        tokenStream = new ShingleFilter(tokenStream, 2);

        List<String> terms = new ArrayList<>();

        CharTermAttribute token = tokenStream.getAttribute(CharTermAttribute.class);
        try {
            tokenStream.reset();
            while (tokenStream.incrementToken()) {
                String term = token.toString();
                if (term.contains("_"))
                    continue;
                if (term.contains(" "))
                    term = term.replace(" ", "+");
                terms.add(term);
            }
            tokenStream.end();
            tokenStream.close();
        } catch (IOException e) {
            logger.error("Error", e);
        }

        return terms;
    }

    public List<String> removeStopWords(String input) {
        String detectedLanguage = detectLanguage(input);
        return removeStopWords(input, detectedLanguage);
    }

    public List<String> removeStopWords(String input,
                                        String language) {

        // tokenize input
        TokenStream tokenStream = new StandardTokenizer(Version.LUCENE_4_9, new StringReader(input));
        // to lowercase
        tokenStream = new LowerCaseFilter(Version.LUCENE_4_9, tokenStream);

        // remove dots from acronyms (and "'s" but already done manually above)
        tokenStream = new ClassicFilter(tokenStream);
        // convert any char to ASCII
        tokenStream = new ASCIIFoldingFilter(tokenStream);

        Set<String> languages = new HashSet<>();
        if (language == null) {
            languages.addAll(defaultStopSets.keySet());
        } else {
            languages.add(language);
        }
        for (String lang : languages) {
            // language default stop words
            tokenStream = new StopFilter(Version.LUCENE_4_9, tokenStream, defaultStopSets.get(lang));
            // language customized stop words
            tokenStream = new StopFilter(Version.LUCENE_4_9, tokenStream, customStopSets.get(lang));
        }
        // customized stop words
        tokenStream = new StopFilter(Version.LUCENE_4_9, tokenStream, CustomStopWords.getStopSet());

        List<String> terms = new ArrayList<>();

        CharTermAttribute token = tokenStream.getAttribute(CharTermAttribute.class);
        try {
            tokenStream.reset();
            while (tokenStream.incrementToken()) {
                String term = token.toString();
                terms.add(term);
            }
            tokenStream.end();
            tokenStream.close();
        } catch (IOException e) {
            logger.error("Error", e);
        }

        return terms;
    }

    public String stem(String term) {

        if (!term.contains("+")) {
            return singleStem(term);
        }
        String[] words = term.split("\\+");
        String[] stems = new String[words.length];
        for (int i=0; i < words.length; i++) {
            String word = words[i];
            String stem = stem(word);
            if (stem == null) {
                return null;
            }
            stems[i] = stem;
        }
        return String.join("+", stems);
    }

    private LinkedHashMap<String, Keyword> convertTermsIntoKeywords(List<String> terms) {

        LinkedHashMap<String, Keyword> keywords = new LinkedHashMap<>();
        for (String term : terms) {
            // stem each term
            String stem = stem(term);
            if (stem == null)
                continue;
            Keyword keyword = keywords.get(stem);
            if (keyword == null) {
                keyword = new Keyword(stem);
                keywords.put(keyword.getStem(), keyword);
            }
            // add its corresponding initial token
            keyword.add(term);
        }
        return keywords;
    }

    // stem a word -> https://en.wikipedia.org/wiki/Word_stem
    private String singleStem(String term) {

        TokenStream tokenStream = null;
        try {
            // tokenize
            tokenStream = new ClassicTokenizer(Version.LUCENE_4_9, new StringReader(term));
            // stem
            tokenStream = new PorterStemFilter(tokenStream);

            // add each token in a set, so that duplicates are removed
            Set<String> stems = new HashSet<>();
            CharTermAttribute token = tokenStream.getAttribute(CharTermAttribute.class);
            tokenStream.reset();
            while (tokenStream.incrementToken()) {
                stems.add(token.toString());
            }
            tokenStream.end();
            tokenStream.close();
            // if no stem or 2+ stems have been found, return null
            if (stems.size() != 1) {
                return null;
            }
            String stem = stems.iterator().next();
            // if the stem has non-alphanumerical chars, return null
            if (!stem.matches("[a-zA-Z0-9-]+")) {
                return null;
            }
            if (tokenStream != null) {
                tokenStream.close();
            }

            return stem;

        } catch (IOException e) {
            logger.error("Error stemming word " + term, e);
            return null;
        }

    }

}