package me.relevante.nlp.core;

import org.springframework.data.annotation.Transient;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
 * One keyword for one stem. 
 * Different words may have the same stem, hence the terms set. 
 * The keyword frequency is incremented every time a new term is found 
 * (even if it has been already found - a set automatically removes duplicates).
 */

public class Keyword {

    private String stem;
    private double tf;
    private double idf;
    @Transient
    private List<String> terms;

    public Keyword() {
        super();
        this.terms = new ArrayList<>();
        this.tf = 0.0;
        this.idf = 0.0;
	  }

	public Keyword(String stem) {
        this();
		this.stem = stem;
	}

    public void add(String term) {
        terms.add(term);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        else if (!(obj instanceof Keyword)) {
            return false;
        }
        else {
            return stem.equals(((Keyword) obj).stem);
        }
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(new Object[] { stem });
    }

    public String getStem() {
        return stem;
    }

    public List<String> getTerms() {
        return terms;
    }

    public double getTf() {
        return tf;
    }

    public double getIdf() {
        return idf;
    }

    public double getTfIdf() {
        return tf * idf;
    }

    public void setTfIdf(double tf, double idf) {
        this.tf = tf;
        this.idf = idf;
    }

    @Override
    public String toString() {
        return "Keyword{" +
                "stem='" + stem + '\'' +
                ", terms=" + terms +
                ", tf=" + tf +
                ", idf=" + idf +
                '}';
    }
}