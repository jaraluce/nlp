package me.relevante.nlp.core;

public class Feature {

    private String stem;
    private double score;

    public Feature() {
        this(null, 0);
    }

    public Feature(String stem,
                   double score) {
        this.stem = stem;
        this.score = score;
	}

    public String getStem() {
        return stem;
    }

    public double getScore() {
        return score;
    }
}