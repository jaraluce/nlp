package me.relevante.nlp.core;

public class ScoredUser {
    private String id;
    private double score;

    public ScoredUser(String id,
                      double score) {
        this.id = id;
        this.score = score;
    }

    private ScoredUser() {
    }

    public String getId() {
        return id;
    }

    public double getScore() {
        return score;
    }
}
